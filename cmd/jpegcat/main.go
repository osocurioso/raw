package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"

	"bitbucket.org/osocurioso/raw/jpeg"
)

var verbose = false

func init() {
	flag.BoolVar(&verbose, "v", false, "verbose output")
	flag.Parse()
}

type dec struct {
	hufTab    [jpeg.MaxTables]jpeg.HTable
	frameInfo jpeg.Frame
	byteIdx   int  // internal counter of printed bytes for line wrapping
	byteOut   bool // Set to true whenever SOS is encountered to handle MCU data
}

func (d *dec) Error(e error) {
	if e != io.EOF {
		log.Panic(e)
	}
}

func (d *dec) Println(args ...interface{}) {
	fmt.Println(args...)
}

func (d *dec) Printf(format string, args ...interface{}) {
	fmt.Printf(format, args...)
}

func (d *dec) Marker(m jpeg.Marker, args ...interface{}) {
	if d.byteOut {
		d.Println()
	}
	d.byteOut = false
	switch m {
	case jpeg.SOS:
		d.byteOut = true
		d.scan(m, args[0].(jpeg.Scan))
	case jpeg.SOF0, jpeg.SOF3:
		d.frame(m, args[0].(jpeg.Frame))
	case jpeg.DQT:
		d.dqt(m, args[0].(jpeg.QTable))
	case jpeg.DHT:
		d.dht(m, args[0].(jpeg.HTable))
	default:
		d.marker(m, args...)
	}
}

func (d *dec) Byte(c byte) {
	if !d.byteOut {
		return // only print byte data when in MCU reading mode (right after SOS)
	}
	if d.byteIdx%24 == 0 {
		d.Println() // wrap long lines in hex output
	}
	d.Printf(" %02x", c)
	d.byteIdx++
}

func (d *dec) showMarker(m jpeg.Marker) {
	d.Println(m)
}

func (d *dec) frame(m jpeg.Marker, f jpeg.Frame) {
	d.showMarker(m)
	d.Printf(" Precision: %d\n", f.P)
	d.Printf(" X: %d; Y: %d\n", f.X, f.Y)
	for _, c := range f.Comps {
		d.Printf(" C:%d, H:%d, V:%d, Tq:%d\n", c.C, c.H, c.V, c.Tq)
	}
	d.frameInfo = f
}

func (d *dec) scan(m jpeg.Marker, s jpeg.Scan) {
	d.showMarker(m)
	d.Printf(" Spectral selection: %d-%d\n", s.Ss, s.Se)
	d.Printf(" Successive approximation bit: hi=%d, lo=%d\n", s.Ah, s.Al)
	for j, c := range s.Comps {
		d.Printf(" Scan component %d:%d, DC table:%d, AC table:%d\n", j, c.Cs, c.Td, c.Ta)
	}
}

func (d *dec) dqt(m jpeg.Marker, q jpeg.QTable) {
	d.showMarker(m)
	d.Printf(" Precision: %d\n", 8+8*q.Pq)
	d.Printf(" Destination: %d\n", q.Tq)
	for i := 0; i < len(q.Q); i += 8 {
		d.Printf(" %#.02x: %#.02x\n", i, q.Q[i:i+8])
	}
}

func (d *dec) dht(m jpeg.Marker, h jpeg.HTable) {
	d.showMarker(m)
	if h.Tc == 1 {
		d.Println(" Class: AC")
	} else {
		d.Println(" Class: DC or lossless")
	}
	d.Println(" Destination:", h.Th)
	d.Println(" Bits:", h.Bits)
	d.Println(" Vals:", h.Vals[:])
	d.hufTab[h.Th] = h
}

func (d *dec) marker(m jpeg.Marker, args ...interface{}) {
	if verbose {
		d.Printf("%s: [%d]\n", m, len(args))
		for _, a := range args {
			d.Printf(" %#v\n", a)
		}
	} else {
		d.showMarker(m)
	}
}

func main() {
	var r io.Reader = os.Stdin
	var d dec
	jpeg.Decode(bufio.NewReader(r), &d)
	//	d.PrintBytes()
}
