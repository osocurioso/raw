// tiffstruct parses and displays the internal structure of the TIFF file
package main

import (
	"crypto/md5"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"bitbucket.org/osocurioso/raw/tiff"
)

var doCheckSum = false
var extractExif = false
var verbose = false

func init() {
	flag.BoolVar(&extractExif, "exif", false, "print Exif information if present")
	flag.BoolVar(&doCheckSum, "checksum", false, "calculate MD5 sum of BYTE and UNDEFINED fields")
	flag.BoolVar(&verbose, "v", false, "verbose output")
}

func main() {
	defer func() {
		if e := recover(); e != nil {
			log.Println("Error:", e)
		}
	}()

	flag.Parse()

	filename := ""
	if flag.NArg() > 0 {
		filename = flag.Args()[0]
	}

	var r io.ReaderAt = os.Stdin
	if flag.NArg() > 0 {
		filename = flag.Args()[0]
		f, err := os.Open(filename)
		if err != nil {
			panic(err)
		}
		r = f
		defer f.Close()
	}

	f, err := tiff.Read(r)
	if err != nil {
		panic(err)
	}

	if verbose {
		fmt.Printf("0x%.8x: Header: %s\n", 0, f.Header.ByteOrder)
	}
	for ifd := f.IFD; ifd != nil; ifd = ifd.Next {
		printIFD(f, ifd, 0)
	}
}

func printIFD(f *tiff.File, ifd *tiff.IFD, level int) {
	exifCode, _ := tiff.Private.GetCode("Exif IFD")
	if verbose {
		pLevel(0, "0x%.8x: ", ifd.Offset)
		pLevel(level, "IFD{nEntries: %d}:\n", len(ifd.Entries))
	}
	for i, e := range ifd.Entries {
		if verbose {
			pLevel(0, "0x%.8x:  ", ifd.Offset+int64(12*i))
			pLevel(level, "%#v [%s]: ", e.Tag, e.Type)
		}
		pLevel(level, "%s: ", e.Tag)
		pLevel(level, toString(*f, e))
		fmt.Println()
		if extractExif && e.Tag == exifCode {
			exifIFD := f.ReadIFD(f.Offset(e.Value[:]))
			printIFD(f, exifIFD, level+1)
		}
	}
	return
}

func pLevel(level int, format string, d ...interface{}) {
	fmt.Printf(strings.Repeat(" ", level))
	fmt.Printf(format, d...)
}

func toString(f tiff.File, e tiff.Entry) string {
	switch e.Type {
	case tiff.ASCII:
		s, err := Ascii(f, e)
		if err == nil {
			return fmt.Sprintf("[%s]", s)
		}
	case tiff.BYTE, tiff.UNDEFINED:
		if doCheckSum {
			b := make([]byte, e.ValueByteSize())
			r := f.EntryReader(e)
			if n, err := r.ReadAt(b, 0); err == nil {
				h := md5.New()
				h.Write(b)
				return fmt.Sprintf("[len=%d,md5=%x]", n, h.Sum(nil))
				//pLevel(0, "[%d]: %v", len(b), b[:n])
			} else {
				return fmt.Sprintf("%v %v", n, err)
			}
		} else {
			return fmt.Sprintf("[ptr=%t,len=%d] %v", e.ValueIsOffset(), e.ValueByteSize(), e.Value[:])
		}
	case tiff.SHORT:
		s, err := Shorts(f, e)
		if err == nil {
			return fmt.Sprintf("%v", s)
		}
	case tiff.LONG:
		s, err := Longs(f, e)
		if err == nil {
			return fmt.Sprintf("%v", s)
		}
	case tiff.RATIONAL:
		s, err := Rationals(f, e)
		if err == nil {
			return fmt.Sprintf("%v", s)
		}
	case tiff.SRATIONAL:
		s, err := Srationals(f, e)
		if err == nil {
			return fmt.Sprintf("%v", s)
		}
	}
	return fmt.Sprintf("%v", e.Value[:])
}

func Bytes(f tiff.File, e tiff.Entry) ([]tiff.Byte, error) {
	buf, err := f.Bytes(e)
	if err != nil {
		return nil, err
	}
	sbuf := make([]tiff.Byte, e.Count)
	for i := 0; i < int(e.Count); i++ {
		sbuf[i] = tiff.Byte(buf[i])
	}
	return sbuf, nil
}

func Ascii(f tiff.File, e tiff.Entry) ([]tiff.Ascii, error) {
	buf, err := f.Bytes(e)
	if err != nil {
		return nil, err
	}
	sbuf := make([]tiff.Ascii, e.Count)
	for i := 0; i < int(e.Count); i++ {
		sbuf[i] = tiff.Ascii(buf[i])
	}
	return sbuf, nil
}

func Shorts(f tiff.File, e tiff.Entry) ([]tiff.Short, error) {
	buf, err := f.Bytes(e)
	if err != nil {
		return nil, err
	}
	elemSize := int(e.Type.ByteSize())
	sbuf := make([]tiff.Short, e.Count)
	for i := 0; i < int(e.Count); i++ {
		b := buf[elemSize*i:]
		sbuf[i] = f.Short(b)
	}
	return sbuf, nil
}

func Longs(f tiff.File, e tiff.Entry) ([]tiff.Long, error) {
	buf, err := f.Bytes(e)
	if err != nil {
		return nil, err
	}
	elemSize := int(e.Type.ByteSize())
	sbuf := make([]tiff.Long, e.Count)
	for i := 0; i < int(e.Count); i++ {
		b := buf[elemSize*i:]
		sbuf[i] = f.Long(b)
	}
	return sbuf, nil
}

func Rationals(f tiff.File, e tiff.Entry) ([]tiff.Rational, error) {
	buf, err := f.Bytes(e)
	if err != nil {
		return nil, err
	}
	elemSize := int(e.Type.ByteSize())
	sbuf := make([]tiff.Rational, e.Count)
	for i := 0; i < int(e.Count); i++ {
		b := buf[elemSize*i:]
		sbuf[i] = f.Rational(b)
	}
	return sbuf, nil
}

func Srationals(f tiff.File, e tiff.Entry) ([]tiff.Srational, error) {
	buf, err := f.Bytes(e)
	if err != nil {
		return nil, err
	}
	elemSize := int(e.Type.ByteSize())
	sbuf := make([]tiff.Srational, e.Count)
	for i := 0; i < int(e.Count); i++ {
		b := buf[elemSize*i:]
		sbuf[i] = f.Srational(b)
	}
	return sbuf, nil
}
