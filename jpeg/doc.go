// Package jpeg provides a low level decoder framework supporting the
// entire ITU-T T.81 JPEG specification.
//
// The  JPEG specification can be found at
// http://www.w3.org/Graphics/JPEG/itu-t81.pdf.
package jpeg
