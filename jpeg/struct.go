package jpeg

// A Frame specifies the metadata associated with the SOF? markers
type Frame struct {
	Type  Marker
	P     int         // sample precision
	Y     int         // number of lines
	X     int         // number of samples per line
	Comps []FrameComp // image components in frame
}

// A FrameComp
type FrameComp struct {
	C  int // component identifier
	H  int // horizontal sampling factor
	V  int // vertical sampling factor
	Tq int // quantization table destination selector
}

// A Scan specifies the metadata associated with the SOS marker
type Scan struct {
	Comps []ScanComp // scan component-specification parameters
	Ss    int        // Start of spectral or predictor selector
	Se    int        // End of spectral selection
	Ah    int        // Successive approximation bit position high
	Al    int        // Successive approximation bit position low or point transform
}

// A ScanComp describes the component associated with a given sample
type ScanComp struct {
	Cs int // Scan component selector
	Td int // DC entropy coding table destination selector
	Ta int // AC entropy coding table destination selector
}

// Expand
type Exp struct {
	Eh int // Expand horizontally
	Ev int // Expand vertically
}

// Number of lines
type Nl int

// Application data segment
type AppData struct {
	N  int    // Kind
	Ap []byte // Application data bytes
}

// Comment segment
type Comment []byte

// Restart interval
type RstInt int

// Arithmetic conditioning table
type ATable struct {
	Tb int // Table class: 0 = DC table or lossless table, 1 = AC table.
	Tc int // Table destination identifier
	Cs int // Conditioning table value
}

// Quantization table
type QTable struct {
	Pq int     // Element precision
	Tq int     // Destination identifier
	Q  [64]int // Elements
}

// Huffman table
type HTable struct {
	Tc   int    // Table class: 0 = DC table or lossless table, 1 = AC table
	Th   int    // Destination identifier
	Bits []byte // Number of Huffman codes of length i
	Vals []byte // Value associated with each Huffman code
}
