package jpeg

import (
	"bytes"
	"testing"
)

type logDec struct {
	*testing.T
	curr Marker
}

func (d logDec) Error(e error) {
	d.Logf("Encountered error %s: ", e)
}
func (d logDec) Marker(m Marker, a ...interface{}) {
	d.curr = m
	switch m {
	case SOF0:
		d.frame(a[0].(Frame))
	default:
		d.Logf("Received marker: %s", m)
	}
}
func (d logDec) Byte(c byte) {
	d.Logf("%s: %x", d.curr, c)
}
func (d logDec) frame(f Frame) {
	d.Logf("Received frame: %s{P:%d,Y:%d,X:%d,Nf:%d}", f.Type, f.P, f.Y, f.X, len(f.Comps))
}

func TestDecoder(t *testing.T) {
	testData := []byte{0xff, SOI,
		0xff, SOF0,
		0x00, 0x0b, // Lf
		8,     // P
		0, 16, // Y
		0, 8, // X
		0x01, // Nf
		0x00, // C1
		0x11, // H1, V1
		0x00, // Tq1
		0xff, 0xff, 0x00, 0xc3, 0x43, 0xff, EOI}
	buf := bytes.NewBuffer(testData)
	l := logDec{t, 0}
	Decode(buf, l)
	//t.Fail()
}
