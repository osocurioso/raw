package jpeg

import (
	"fmt"
)

// A byteReader is used to read a byte stream filtered according to the JPEG spec
// regarding markers, fill bytes, and stuff bytes.
type byteReader struct {
	Reader
	prev byte
	err  error
}

// A markerError is returned as the error from (*ByteReader).ReadByte when the current byte is a marker.
type markerError Marker

func (e markerError) Error() string {
	return fmt.Sprintf("Encountered marker %s", Marker(e))
}

// Read next byte, but handle fill (0xff) and stuff (0x00) bytes internally.
// Returns markerError in err if c was preceeded by a 0xff.
func (r *byteReader) NextByte() (c byte, isMrk bool, err error) {
	if r.err != nil {
		return 0, false, r.err
	}
	advance := func() {
		c, r.err = r.Reader.ReadByte()
	}
	for advance(); (c == 0xff) && (r.err == nil); advance() {
		r.prev = c
	}
	if r.prev == 0xff && c == 0x00 {
		c = 0xff
		r.prev = 0x00
		return
	}
	if r.prev == 0xff {
		r.prev = c
		isMrk = true
	}
	return
}
