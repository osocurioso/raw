package jpeg

import (
	"bytes"
	"io"
	"testing"
)

var testData = []byte{0x54, 0xff, 0xff, 0xda, 0x45, 0xff, 0x64, 0xff, 0xff, 0x00, 0xc3, 0x43, 0xff, 0x35, 0xff}

func TestReadByte(t *testing.T) {
	buf := bytes.NewBuffer(testData)
	b := &byteReader{Reader: buf}
	expect := func(exp byte, expMrk bool) {
		c, isMrk, err := b.NextByte()
		match := (exp == c) && (expMrk == isMrk)
		t.Logf("Expected (%#x,%t); Got (%#x,%t); Match: %t", exp, expMrk, c, isMrk, match)
		if expMrk {
			if !isMrk {
				t.Errorf("Expected %#x to be marker but %#x is not marked as such", exp, c)
			}
		} else if err != nil {
			t.Errorf("For (%#x,%t), got unexpected error: %s", exp, expMrk, err)
		} else if c != exp {
			t.Errorf("Expected %#x but got %#x", exp, c)
		}
	}
	expect(0x54, false)
	expect(0xda, true)
	expect(0x45, false)
	expect(0x64, true)
	expect(0xff, false)
	expect(0xc3, false)
	expect(0x43, false)
	expect(0x35, true)
	expect(0xff, false)
	_, err := b.ReadByte()
	if err != io.EOF {
		t.Fatal("Expected io.EOF")
	}
}
