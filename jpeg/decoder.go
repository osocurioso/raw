package jpeg

import (
	"fmt"
	"io"
)

type fError string

func (err fError) Error() string {
	return "Format Error: " + string(err)
}

const (
	MaxTables = 4
)

type decoder struct {
	r        byteReader
	tmp      [1024]byte
	offset   int
	marker   Marker
	len      int
	frame    *Frame
	scan     *Scan
	ri       RstInt
	qtables  [MaxTables]*QTable
	htables  [MaxTables]*HTable
	actables [MaxTables]*ATable
}

type Decoder interface {
	Error(error)
	Marker(m Marker, a ...interface{})
	Byte(b byte)
}

type Reader interface {
	io.Reader
	io.ByteReader
}

func Decode(rr Reader, d Decoder) {
	var tmp [1024]byte
	r := byteReader{Reader: rr}
	//for i := 0; i < 1000; i++ {
	for {
		code, isMrk, err := r.NextByte()
		if err != nil {
			d.Error(err)
			break
		}
		if isMrk {
			m := Marker(code)
			if m.HasSegment() {
				readSegment(d, m, tmp[:], r)
			} else {
				d.Marker(m)
			}
		} else {
			d.Byte(code)
		}
	}
}

func readFull(r io.Reader, buf []byte) {
	_, err := io.ReadFull(r, buf)
	if err != nil {
		panic(err)
	}
}

func readSegment(d Decoder, m Marker, tmp []byte, r byteReader) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = e.(error)
		}
	}()
	readFull(r, tmp[:2])
	l := getUint16(tmp[:2]) - 2
	//fmt.Printf("m=%s,len=%d,%x\n", m, l, tmp[:2])
	readFull(r, tmp[:l])
	switch m {
	case DHT:
		readDHT(d, m, tmp[:l])
	case DQT:
		readDQT(d, m, tmp[:l])
	case SOS:
		readScan(d, m, tmp[:l])
	case DRI:
		readDRI(d, m, tmp[:l])
	case DAC:
		readDAC(d, m, tmp[:l])
	case SOF0, SOF1, SOF2, SOF3, SOF5, SOF6, SOF7, SOF9, SOF10, SOF11, SOF13, SOF14, SOF15:
		readFrame(d, m, tmp[:l])
	case APP0, APP1, APP2, APP3, APP4, APP5, APP6, APP7, APP8, APP9, APP10, APP11, APP12, APP13, APP14, APP15:
		readAPP(d, m, tmp[:l])
	case COM:
		readCOM(d, m, tmp[:l])
	case DNL:
		readDNL(d, m, tmp[:l])
	case EXP:
		readEXP(d, m, tmp[:l])
	}
	return nil
}

func readFrame(d Decoder, m Marker, buf []byte) {
	var f Frame
	f.Type = m
	f.P = getUint8(buf)
	buf = buf[1:]
	f.Y = getUint16(buf)
	buf = buf[2:]
	f.X = getUint16(buf)
	buf = buf[2:]
	Nf := getUint8(buf)
	buf = buf[1:]
	f.Comps = make([]FrameComp, Nf)
	for i := 0; i < Nf; i++ {
		f.Comps[i].C = getUint8(buf)
		buf = buf[1:]
		f.Comps[i].H, f.Comps[i].V = getNibbles(buf)
		buf = buf[1:]
		f.Comps[i].Tq = getUint8(buf)
		buf = buf[1:]
	}
	//fmt.Println(m, f, buf)
	d.Marker(m, f)
}

func readScan(d Decoder, m Marker, buf []byte) {
	var s Scan
	Ns := getUint8(buf)
	buf = buf[1:]
	s.Comps = make([]ScanComp, Ns)
	for i := 0; i < Ns; i++ {
		s.Comps[i].Cs = getUint8(buf[0:])
		s.Comps[i].Td, s.Comps[i].Ta = getNibbles(buf[1:])
		buf = buf[2:]
	}
	s.Ss = getUint8(buf[0:])
	s.Se = getUint8(buf[1:])
	s.Ah, s.Al = getNibbles(buf[2:])
	//fmt.Println(m, s, buf)
	d.Marker(m, s)
}

func readDQT(d Decoder, m Marker, buf []byte) {
	for {
		if len(buf) == 0 {
			break
		}
		var q QTable
		q.Pq, q.Tq = getNibbles(buf)
		buf = buf[1:]
		for i := 0; i < 64; i++ {
			if q.Pq == 0 {
				q.Q[i] = getUint8(buf[i:]) // 8-bit quantization elements
			} else {
				q.Q[i] = getUint16(buf[2*i:])
			}
		}
		if q.Pq == 0 {
			buf = buf[64:]
		} else {
			buf = buf[128:]
		}
		//fmt.Printf("%#v\n", q)
		d.Marker(m, q)
	}
}

func readDHT(d Decoder, m Marker, buf []byte) {
	for {
		if len(buf) == 0 {
			break
		}
		var h HTable
		h.Tc, h.Th = getNibbles(buf)
		buf = buf[1:]
		h.Bits = buf[0:16]
		buf = buf[16:]
		nSymbols := 0
		for _, repeat := range h.Bits {
			nSymbols += int(repeat)
		}
		h.Vals = buf[:nSymbols]
		buf = buf[nSymbols:]
		//fmt.Printf("%#v\n", h)
		//printHuff(h.Bits, h.Diff)
		d.Marker(m, h)
	}
}

func readDAC(d Decoder, m Marker, buf []byte) {
	for {
		if len(buf) == 0 {
			break
		}
		var ac ATable
		ac.Tb, ac.Tc = getNibbles(buf)
		ac.Cs = getUint8(buf[1:])
		buf = buf[2:]
		//fmt.Printf("%#v\n", ac)
		d.Marker(m, ac)
	}
}

func readDRI(d Decoder, m Marker, buf []byte) {
	ri := RstInt(buf[0])
	//fmt.Println("%#v\n", ri)
	d.Marker(m, ri)
}

func readCOM(d Decoder, m Marker, buf []byte) {
	c := Comment(buf)
	d.Marker(m, c)
	//fmt.Printf("%#v\n", c)
}

func readAPP(d Decoder, m Marker, buf []byte) {
	var a AppData
	a.N = int(m) - APP0
	a.Ap = buf
	//fmt.Printf("%#v\n", a)
	d.Marker(m, a)
}

func readDNL(d Decoder, m Marker, buf []byte) {
	nl := Nl(buf[0])
	//fmt.Println("%#v\n", nl)
	d.Marker(m, nl)
}

func readEXP(d Decoder, m Marker, buf []byte) {
	var e Exp
	e.Eh, e.Ev = getNibbles(buf)
	//fmt.Println("%#v\n", e)
	d.Marker(m, e)
}

func getUint8(b []byte) int {
	return int(b[0])
}

func getUint16(b []byte) int {
	d := int(b[0])<<8 | int(b[1])
	return d
}

func getNibbles(b []byte) (hi, lo int) {
	hi = int(b[0]&0xf0) >> 4
	lo = int(b[0] & 0x0f)
	return
}

func printHuff(bits, huffval []byte) {
	codeIdx := 0
	codeBits := 1
	for i, repeats := range bits {
		length := i + 1
		for j := 0; j < int(repeats); j++ {
			fmt.Printf("%#.2d: %#.2x represented by %b \n", length, huffval[codeIdx], codeBits)
			codeBits += 1
			codeIdx++
		}
		codeBits *= 2
	}
}
