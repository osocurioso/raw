package jpeg

import (
	"testing"
)

func TestMarkerSegment(t *testing.T) {
	ms := []struct {
		m   Marker
		seg bool
	}{
		{SOF1, true},
		{SOF2, true},
		{SOI, false},
		{EOI, false},
		{RST4, false},
		{SOS, true},
		{DHT, true},
	}

	for _, tc := range ms {
		if tc.m.HasSegment() != tc.seg {
			t.Errorf("Expected %t for %s, but got %t", tc.seg, tc.m, tc.m.HasSegment())
		}
	}
}

func TestMarkerMatch(t *testing.T) {
	buf := []byte("\xff\xc4\x35\x4a\xff\x00\xff\xd7")
	ms := []struct {
		m   Marker
		pos int
	}{
		{DHT, 0},
		{RST7, 6},
	}

	for _, tc := range ms {
		if !tc.m.Match(buf[tc.pos:]) {
			t.Errorf("Expected match for %s at %#x!", tc.m, tc.pos)
		}
	}
}

func TestMarkerNaming(t *testing.T) {
	ms := []struct {
		m    Marker
		name string
	}{
		{SOI, "SOI"},
		{EOI, "EOI"},
		{SOS, "SOS"},
		{DRI, "DRI"},
		{DHT, "DHT"},
		{DQT, "DQT"},
		{DAC, "DAC"},
		{DNL, "DNL"},
		{COM, "COM"},
		{RST5, "RST5"},
		{APP0, "APP0"},
		{APP15, "APP15"},
	}
	for _, tc := range ms {
		if tc.m.String() != tc.name {
			t.Errorf("Expected %s for marker %#x, but got %s", tc.name, byte(tc.m), tc.m.String())
		}
	}
}
