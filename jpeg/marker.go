package jpeg

import (
	"fmt"
)

// A Marker represents the 0xPQ part of a two-byte JPEG marker 0xFFPQ.
type Marker byte

// HasSegment returns false if the Marker is not followed by a fixed length marker segment.
func (m Marker) HasSegment() bool {
	switch m {
	case SOI, EOI:
		return false
	case RST0, RST1, RST2, RST3, RST4, RST5, RST6, RST7:
		return false
	case TEM:
		return false
	default:
		return true
	}
}

func (m Marker) String() string {
	if nm, ok := mrkNames[m]; ok {
		return nm
	}
	return fmt.Sprintf("%#x", byte(m))
}

func (m Marker) Match(buf []byte) bool {
	if len(buf) < 2 {
		return false
	}
	return buf[0] == 0xff && buf[1] == byte(m)
}

// Markers with * have no associated segment.
const (
	SOF0  = 0xc0 // Baseline DCT
	SOF1  = 0xc1 // Extended sequential DCT
	SOF2  = 0xc2 // Progressive DCT
	SOF3  = 0xc3 // Lossless (sequential)
	SOF5  = 0xc5 // Differential sequential DCT
	SOF6  = 0xc6 // Differential progressive DCT
	SOF7  = 0xc7 // Differential lossless (sequential)
	JPG   = 0xc8 // Reserved for JPEG extensions
	SOF9  = 0xc9 // Extended sequential DCT
	SOF10 = 0xca // Progressive DCT
	SOF11 = 0xcb // Lossless (sequential)
	SOF13 = 0xcd // Differential sequential DCT
	SOF14 = 0xce // Differential progressive DCT
	SOF15 = 0xcf // Differential lossless (sequential)
	DHT   = 0xc4 // Define Huffman table(s)
	DAC   = 0xcc // Define arithmetic codes conditioning(s)
	RST0  = 0xd0 // Restart interval 'm' modulo 8 *
	RST1  = 0xd1
	RST2  = 0xd2
	RST3  = 0xd3
	RST4  = 0xd4
	RST5  = 0xd5
	RST6  = 0xd6
	RST7  = 0xd7
	SOI   = 0xd8 // Start of image *
	EOI   = 0xd9 // End of image *
	SOS   = 0xda // Start of scan
	DQT   = 0xdb // Define quantization table(s)
	DNL   = 0xdc // Define number of lines
	DRI   = 0xdd // Define restart interval
	DHP   = 0xde // Define hierarchical progression
	EXP   = 0xdf // Expand reference component(s)
	APP0  = 0xe0 // Application segments
	APP1  = 0xe1
	APP2  = 0xe2
	APP3  = 0xe3
	APP4  = 0xe4
	APP5  = 0xe5
	APP6  = 0xe6
	APP7  = 0xe7
	APP8  = 0xe8
	APP9  = 0xe9
	APP10 = 0xea
	APP11 = 0xeb
	APP12 = 0xec
	APP13 = 0xed
	APP14 = 0xee
	APP15 = 0xef
	JPG0  = 0xf0 // Reserved for JPEG extensions
	JPG1  = 0xf1
	JPG2  = 0xf2
	JPG3  = 0xf3
	JPG4  = 0xf4
	JPG5  = 0xf5
	JPG6  = 0xf6
	JPG7  = 0xf7
	JPG8  = 0xf8
	JPG9  = 0xf9
	JPG10 = 0xfa
	JPG11 = 0xfb
	JPG12 = 0xfc
	JPG13 = 0xfd
	COM   = 0xfe // Comment
	TEM   = 0x01 // For temporary private use in arithmetic coding *
)

var mrkNames = map[Marker]string{
	// Start Of Frame markers, non-differential, Huffman coding
	SOF0: "SOF0", // Baseline DCT
	SOF1: "SOF1", // Extended sequential DCT
	SOF2: "SOF2", // Progressive DCT
	SOF3: "SOF3", // Lossless (sequential)

	// Start Of Frame markers, differential, Huffman coding
	SOF5: "SOF5", // Differential sequential DCT
	SOF6: "SOF6", // Differential progressive DCT
	SOF7: "SOF7", // Differential lossless (sequential)

	// Start Of Frame markers, non-differential, arithmetic coding
	JPG:   "JPG",  // Reserved for JPEG extensions
	SOF9:  "SOF9", // Extended sequential DCT
	SOF10: "SOFA", // Progressive DCT
	SOF11: "SOFB", // Lossless (sequential)

	// Start Of Frame markers, differential, arithmetic coding
	SOF13: "SOFD", // Differential sequential DCT
	SOF14: "SOFE", // Differential progressive DCT
	SOF15: "SOFF", // Differential lossless (sequential)

	// Huffman table specification
	DHT: "DHT", // Define Huffman table(s)

	// Arithmetic coding conditioning specification
	DAC: "DAC", // Define arithmetic codes conditioning(s)

	// Restart interval termination
	RST0: "RST0", // Restart with modulo 8 count 0 *
	RST1: "RST1", // Restart with modulo 8 count 1 *
	RST2: "RST2", // Restart with modulo 8 count 2 *
	RST3: "RST3", // Restart with modulo 8 count 3 *
	RST4: "RST4", // Restart with modulo 8 count 4 *
	RST5: "RST5", // Restart with modulo 8 count 5 *
	RST6: "RST6", // Restart with modulo 8 count 6 *
	RST7: "RST7", // Restart with modulo 8 count 7 *

	// Other markers
	SOI: "SOI", // Start of image *
	EOI: "EOI", // End of image *
	SOS: "SOS", // Start of scan
	DQT: "DQT", // Define quantization table(s)
	DNL: "DNL", // Define number of lines
	DRI: "DRI", // Define restart interval
	DHP: "DHP", // Define hierarchical progression
	EXP: "EXP", // Expand reference component(s)
	COM: "COM", // Comment

	APP0:  "APP0",
	APP1:  "APP1",
	APP2:  "APP2",
	APP3:  "APP3",
	APP4:  "APP4",
	APP5:  "APP5",
	APP6:  "APP6",
	APP7:  "APP7",
	APP8:  "APP8",
	APP9:  "APP9",
	APP10: "APP10",
	APP11: "APP11",
	APP12: "APP12",
	APP13: "APP13",
	APP14: "APP14",
	APP15: "APP15",
	// Reserved markers
	TEM: "TEM", // For temporary private use in arithmetic coding *
}
