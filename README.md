# Access to RAW image formats

This library aims to implement decoders for RAW image formats from various camera vendors. 

Several open source projects have put a lot of effort into reverse engineering these file formats. Notable among these is [dcraw](http://www.cybercom.net/~dcoffin/dcraw/).
