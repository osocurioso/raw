#!/usr/bin/env bash

cat << EOF
package tiff

func init() {
EOF

awk -F'\t' 'NF==4 { print FILENAME":0x"$2":\""$3"\"" }' $@ | \
sed -e 's/\.txt//g' | \
awk -F':' '{ print "\t"$1".Set("$2", "$3")"}'

echo "}"

echo
awk '{ print FILENAME }' $@ \
| sed -e 's/\.txt//g' \
| awk '{ print "var "$1" TagSpace = CreateTagSpace(\""$1"\")"}' \
| uniq
