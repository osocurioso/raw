package tiff

import (
	"fmt"
)

// A TagSpace represents a (named) collection of TIFF tags.
type TagSpace struct {
	name      string
	name2code map[string]TagID
	code2name map[TagID]string
}

// CreateTagSpace returns a new TagSpace labelled name.
func CreateTagSpace(name string) TagSpace {
	var n TagSpace
	n.name = name
	n.name2code = make(map[string]TagID)
	n.code2name = make(map[TagID]string)
	return n
}

// Set associates code and name in the TagSpace ns.
func (ns TagSpace) Set(code TagID, name string) {
	ns.name2code[name] = code
	ns.code2name[code] = name
}

// Returns true if c is in ns.
func (ns TagSpace) HasCode(c TagID) bool {
	_, ok := ns.code2name[c]
	return ok
}

func (ns TagSpace) GetName(c TagID) (string, error) {
	if name, ok := ns.code2name[c]; ok {
		return name, nil
	}
	return "", fmt.Errorf("Unknow TagCode: %d", int(c))
}

func (ns TagSpace) HasName(name string) bool {
	_, ok := ns.name2code[name]
	return ok
}

func (ns TagSpace) GetCode(name string) (TagID, error) {
	if id, ok := ns.name2code[name]; ok {
		return id, nil
	}
	return 0, fmt.Errorf("Unknown tag name: %s", name)
}
