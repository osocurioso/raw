package tiff

func init() {
	BaseLine.Set(0x00FE, "NewSubfileType")
	BaseLine.Set(0x00FF, "SubfileType")
	BaseLine.Set(0x0100, "ImageWidth")
	BaseLine.Set(0x0101, "ImageLength")
	BaseLine.Set(0x0102, "BitsPerSample")
	BaseLine.Set(0x0103, "Compression")
	BaseLine.Set(0x0106, "PhotometricInterpretation")
	BaseLine.Set(0x0107, "Threshholding")
	BaseLine.Set(0x0108, "CellWidth")
	BaseLine.Set(0x0109, "CellLength")
	BaseLine.Set(0x010A, "FillOrder")
	BaseLine.Set(0x010E, "ImageDescription")
	BaseLine.Set(0x010F, "Make")
	BaseLine.Set(0x0110, "Model")
	BaseLine.Set(0x0111, "StripOffsets")
	BaseLine.Set(0x0112, "Orientation")
	BaseLine.Set(0x0115, "SamplesPerPixel")
	BaseLine.Set(0x0116, "RowsPerStrip")
	BaseLine.Set(0x0117, "StripByteCounts")
	BaseLine.Set(0x0118, "MinSampleValue")
	BaseLine.Set(0x0119, "MaxSampleValue")
	BaseLine.Set(0x011A, "XResolution")
	BaseLine.Set(0x011B, "YResolution")
	BaseLine.Set(0x011C, "PlanarConfiguration")
	BaseLine.Set(0x0120, "FreeOffsets")
	BaseLine.Set(0x0121, "FreeByteCounts")
	BaseLine.Set(0x0122, "GrayResponseUnit")
	BaseLine.Set(0x0123, "GrayResponseCurve")
	BaseLine.Set(0x0128, "ResolutionUnit")
	BaseLine.Set(0x0131, "Software")
	BaseLine.Set(0x0132, "DateTime")
	BaseLine.Set(0x013B, "Artist")
	BaseLine.Set(0x013C, "HostComputer")
	BaseLine.Set(0x0140, "ColorMap")
	BaseLine.Set(0x0152, "ExtraSamples")
	BaseLine.Set(0x8298, "Copyright")
}

var BaseLine TagSpace = CreateTagSpace("BaseLine")
