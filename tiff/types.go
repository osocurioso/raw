package tiff

import (
	"fmt"
)

const (
	_            = iota
	BYTE TagType = iota
	ASCII
	SHORT
	LONG
	RATIONAL
	SBYTE
	UNDEFINED
	SSHORT
	SLONG
	SRATIONAL
	FLOAT
	DOUBLE
	IFDPTR
)

func (t TagType) String() string {
	switch t {
	case BYTE:
		return "BYTE"
	case ASCII:
		return "ASCII"
	case SHORT:
		return "SHORT"
	case LONG:
		return "LONG"
	case RATIONAL:
		return "RATIONAL"
	case SBYTE:
		return "SBYTE"
	case UNDEFINED:
		return "UNDEFINED"
	case SSHORT:
		return "SSHORT"
	case SLONG:
		return "SLONG"
	case SRATIONAL:
		return "SRATIONAL"
	case FLOAT:
		return "FLOAT"
	case DOUBLE:
		return "DOUBLE"
	case IFDPTR:
		return "IFDPTR"
	default:
		return "!UNKNOWN!"
	}
}

func (t TagType) ByteSize() int64 {
	switch t {
	case BYTE, ASCII, SBYTE, UNDEFINED:
		return 1
	case SHORT, SSHORT:
		return 2
	case LONG, SLONG, FLOAT, IFDPTR:
		return 4
	case RATIONAL, SRATIONAL, DOUBLE:
		return 8
	default:
		return 1
	}
}

type Byte uint8
type Ascii uint8
type Short uint16
type Long uint32
type Rational struct {
	Numer, Denom Long
}

type Sbyte int8
type Undefined uint8
type Sshort int16
type Slong int32
type Srational struct {
	Numer, Denom Slong
}
type Float float32
type Double float64

func (r Rational) String() string {
	return fmt.Sprintf("%d/%d", r.Numer, r.Denom)
}

func (r Srational) String() string {
	return fmt.Sprintf("%d/%d", r.Numer, r.Denom)
}
