// Package tiff provides support for decoding the structure of TIFF 6.0 files.
//
// The specification can be found at http://www.exif.org/TIFF6.pdf.
package tiff

import (
	"encoding/binary"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"runtime"
)

var logger = log.New(ioutil.Discard, log.Prefix(), log.Flags())

// A File represents a decoded TIFF file.
type File struct {
	*Header
	io.ReaderAt
	IFD *IFD
}

// Read attempts to decode the provided io.ReaderAt according to the TIFF spec.
func Read(r io.ReaderAt) (*File, error) {
	var h Header

	_, err := h.decode(r)
	if err != nil {
		return nil, err
	}

	f := &File{Header: &h, ReaderAt: r}
	f.IFD = f.ReadIFD(f.FirstIFD)
	return f, nil
}

// ReadIFD decodes the IFD structure of the File recursively until no more IFD's can be found.
func (f File) ReadIFD(offset int64) *IFD {
	return readIFD(f, offset, f.ByteOrder)
}

func readIFD(r io.ReaderAt, offset int64, byteOrder ByteOrder) (dir *IFD) {
	var b = make([]byte, 12)
	_, err := r.ReadAt(b[:2], offset)
	if err != nil {
		panic(err)
	}
	nEntries := int(byteOrder.Uint16(b[0:2]))
	var ifd IFD
	ifd.Offset = offset
	ifd.Entries = make([]Entry, nEntries)
	for i := 0; i < nEntries; i++ {
		eoffset := offset + 2 + int64(i)*12
		_, err := r.ReadAt(b, eoffset)
		if err != nil {
			panic(err)
		}
		entry := &ifd.Entries[i]
		entry.Offset = eoffset
		entry.Tag = TagID(byteOrder.Uint16(b[0:2]))
		entry.Type = TagType(byteOrder.Uint16(b[2:4]))
		entry.Count = TagCount(byteOrder.Uint32(b[4:8]))
		entry.Value[0] = b[8]
		entry.Value[1] = b[9]
		entry.Value[2] = b[10]
		entry.Value[3] = b[11]
		logger.Printf("IFD 0x%04x [%.2d]: %v", offset, i, *entry)
	}
	_, err = r.ReadAt(b[:4], offset+2+int64(nEntries)*12)
	if err != nil {
		panic(err)
	}
	nextOffset := byteOrder.Offset(b[:4])
	if nextOffset != 0 {
		ifd.Next = readIFD(r, nextOffset, byteOrder)
	}
	// ifd.Next = nil
	dir = &ifd
	return
}

// Bytes returns the Value bytes pointed to by a particular entry.
func (f File) Bytes(e Entry) ([]byte, error) {
	ora := f.EntryReader(e)
	nBytes := e.ValueByteSize()
	buf := make([]byte, nBytes)
	n, err := ora.ReadAt(buf, 0)
	return buf[:n], err
}

type offsetReaderAt struct {
	io.ReaderAt
	offset int64
}

func (ora offsetReaderAt) ReadAt(b []byte, offset int64) (int, error) {
	return ora.ReaderAt.ReadAt(b, ora.offset+offset)
}

// EntryReader returns a ReaderAt which can be used to extract the bytes associated with an Entry.
func (f File) EntryReader(e Entry) io.ReaderAt {
	nBytes := e.ValueByteSize()
	ora := offsetReaderAt{ReaderAt: f, offset: e.Offset + 8}
	if nBytes > 4 {
		ora.offset = f.Offset(e.Value[:])
	}
	ora.ReaderAt = f
	return ora
}

// IFD describes the contents of an Image File Directory entry in a TIFF file.
type IFD struct {
	Offset  int64
	Entries []Entry
	Next    *IFD
}

// A TagType represents the 16 bit code for the different tag types defined by the TIFF spec.
type TagType uint16

// A TagID represents the 16 bit integer identifier for an IFD tag.
type TagID uint16

// Returns the TagID integer value encoded as a hexadecimal.
func (c TagID) GoString() string {
	return fmt.Sprintf("%#.4x", uint16(c))
}

// Returns the TagID name as defined in BaseLine, Extension, Private, Exif, and Gps TagSpaces.
func (c TagID) String() string {
	return c.Lookup(BaseLine, Extension, Private, Exif, Gps)
}

// Return the first name assocated with the TagID in the given TagSpace(s).
func (c TagID) Lookup(spaces ...TagSpace) string {
	for _, ns := range spaces {
		if name, err := ns.GetName(c); err == nil {
			return name
		}
	}
	return c.GoString()
}

// A TagCount describes how many repeated elements there are of e.Type in this Entry.
type TagCount uint32

// A TagValue contains the direct value stored in each TIFF entry.
type TagValue [4]byte

// An Entry contains the data for a single entry or field in an IFD (Image Field Directory).
type Entry struct {
	Offset int64    // Offset of this Entry within its associated ReaderAt
	Tag    TagID    // Tag code
	Type   TagType  // Tag type
	Count  TagCount // How many elements of type Type are covered by this Entry
	Value  TagValue // Value or offset to value if it doesn't fit in 4 bytes
}

// Returns true if e.Value is an offset to where the value data can be found.
func (e Entry) ValueIsOffset() bool {
	return e.ValueByteSize() > 4
}

// ValueByteSize returns the total byte size of e.Entry's Value.
func (e Entry) ValueByteSize() int64 {
	return e.Type.ByteSize() * int64(e.Count)
}

// A ByteOrder represents the byte order of a TIFF file and supports decoding of the various TIFF types.
type ByteOrder struct {
	binary.ByteOrder
}

func (order ByteOrder) ReadIFD(r io.ReaderAt, offset int64) *IFD {
	return readIFD(r, offset, order)
}

// Read and decode 2 bytes as Short
func (order ByteOrder) Short(b []byte) Short {
	return Short(order.Uint16(b[:2]))
}

// Read and decode 4 bytes as Long
func (order ByteOrder) Long(b []byte) Long {
	return Long(order.Uint32(b[:4]))
}
func (order ByteOrder) Slong(b []byte) Slong {
	return Slong(order.Long(b))
}

// Read and decode 2x 2-bytes as a Rational
func (order ByteOrder) Rational(b []byte) (r Rational) {
	r.Numer = order.Long(b[:4])
	r.Denom = order.Long(b[4:])
	return
}

// Read and decode 2x 2-byte as a Srational
func (order ByteOrder) Srational(b []byte) (r Srational) {
	r.Numer = order.Slong(b[:4])
	r.Denom = order.Slong(b[4:])
	return
}

// Read a 4-byte file offset
func (order ByteOrder) Offset(b []byte) int64 {
	return int64(order.Uint32(b))
}

// Header represents a TIFF file header.
type Header struct {
	ByteOrder
	FirstIFD int64 // Offset to first IFD
}

// Decodest the first 8 bytes of r as a TIFF file.
func (h *Header) decode(r io.ReaderAt) (n int, err error) {
	defer func() {
		if e := recover(); e != nil {
			if _, ok := e.(runtime.Error); ok {
				panic(e)
			}
			err = e.(error)
		}
	}()
	const HEADER_SIZE = 8
	b := make([]byte, HEADER_SIZE)
	n, err = r.ReadAt(b, 0)
	if err != nil {
		panic(err)
	}
	if n < HEADER_SIZE {
		panic(fmt.Errorf("Unable to read enough bytes to decode Header"))
	}
	//logger.Printf("Header bytes: %x\n", b)
	if b[0] == 'I' && b[1] == 'I' {
		h.ByteOrder = ByteOrder{binary.LittleEndian}
	} else if b[0] == 'M' && b[1] == 'M' {
		h.ByteOrder = ByteOrder{binary.BigEndian}
	} else {
		panic(fmt.Errorf("Invalid byte order: %x", b[0:2]))
	}
	magic := h.Uint16(b[2:4])
	if magic != 42 {
		panic(fmt.Errorf("Invalid magic number: %d (expected 42)", magic))
	}
	h.FirstIFD = h.Offset(b[4:])
	return
}
